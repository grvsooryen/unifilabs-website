# Unifi Labs Static Website

## Setup

The project contains statics webpages, therefore jekyll is used to generate the static website (Eg. Bootstrap website)

Run the following commands to get started:
```
gem install jekyll bundler
bundle install
bundle exec jekyll serve
```